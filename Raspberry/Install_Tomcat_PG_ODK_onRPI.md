[retour à la page d'Accueil](README.md)

# Installer et configurer PostgreSQL ![pg](images/logo_pg.png) et ODK Aggregate ![odk](images/logo_odk.png) sur Raspberry Pi ![pi](images/logo_rpi.png)

Procédures rassemblées dans ce document de synthèse par Bertrand Houdusse, testé et mis en forme par Sylvain Badey.

## Préparer le raspberry Pi (RPI) ![pi](images/logo_rpi.png)

Nous avons utilisé un Raspberry Pi 3 et une carte SD de 16Go.

Au départ le Raspberry Pi est connecté à un écran (via un adaptateur HDMI<>VGA) et on y a connecté un clavier et une souris en USB.

Il est connecté au réseau INRAP via un câble RJ45.

### Installer le système d'exploitation sur la carte SD

Formater la carte SD avec le logiciel [SD Formatter](https://www.sdcard.org/downloads/formatter/).

Télecharger une version **lite** du système d'explopitation (OS) dédié au RPI sur le site [raspberrypi.org](https://www.raspberrypi.org/downloads/raspbian/)

Dézipper le fichier zip.

Graver le fichier **.img** avec le logiciel [Etcher](https://www.balena.io/etcher/)

### Installation et paramétrage du RPI

Insérer la carte SD et démarrer le RPI branché sur un écran avec un clavier et une souris.

#### Identification et mises à jour

Après quelques secondes de défilement  de lignes, une invite de commande s'affiche

`raspberrypi login:`

Le logiciel attend votre identifiant/*login* tapez `pi`

Ensuite l'invite de commande vous demande le mot de passe

`Password:    `

Le logiciel attend le mot de passe qui est par défaut `raspberry`

> ![Attention](images/attention.png) le clavier est en mode **qwerty** par défaut il faut donc taper **rqspberry**    



> **<u>Note:</u> Pour changer la disposition du clavier**
>
> Taper `sudo raspi-config` (enfin...`sudo rqspi-config`)
>
> Puis avec les flèches ↑↓ aller sur le menu `4 Localisation Options` , valider avec [Entrée]
>
> Choisir l'option `I3 Change Keyboard Layout` [Entrée]
>
> Choisir  `Generic 105-key (Intl) PC` [Entrée]
>
> Choisir `Other` [Entrée], Appuyer sur la touche **F** et choisir `French`[Entrée]
>
> Choisir et valider les options suivantes par défaut:  `French`, puis `The default or the keyboard layout`,  puis `No compose key` 
>
> Enfin, avec la touche → , choisir `<Finish> `[Entrée]



#### Mettre à jour le système d'exploitation avec les commandes

`sudo apt-get update ` [Entrée]

puis

`sudo apt-get upgrade ` [Entrée]

> <u>Note:</u> Il est de bon ton de faire ces 2 commandes régulièrement, du moins avant l'installation de nouveau paquets/programmes.

#### Commander le RPI à distance depuis le PC windows en SSH

Installer le logiciel [Putty](https://www.putty.org/) ![putty](images/logo_putty.png)

Pour connaitre l'adresse IP du RPI taper `ifconfig`dans l'invite de commande est  observer la ligne `inet 10.3.2.239 `cette série de 4 nombres est votre adresse IP.  <u>attention</u> cette IP n'est pas fixe par défaut elle peut changer au redémarrage du PI... voir avec la DSI pour avoir une IP fixe...

> **<u>Note:</u> Pour Activer la connection SSH sur le RPI**
>
> Taper `sudo raspi-config`
>
> Puis avec ↑↓ aller sur le menu `5 Interfacing Options` ,puis `P2 SSH`, `<YES>`et enfin `<OK>`
>
> <u>Note:</u>vous pouvez aussi en profiter pour activer VNC (une interface de connexion à distance) via `P3 VNC` pour l'installer sur le RPI et une installation du [logiciel VNC Viewer](https://www.realvnc.com/fr/connect/download/viewer/windows/) sur votre PC 

Se connecter avec putty en SSH : indiquer l'adresse IP, cocher SSH et cliquer sur le bouton [Open]

![putty_conf](images/1_putty.png)

La **Console** s'affiche, il suffit d'indiquer son *login* et son *Password* pour accéder à l'invite de commande du RPI sur votre PC.

![putty_shell](images/2_putty_shell.png)

**=> Vous avez alors accès au RPI qui n'a plus besoin ni d'écran, ni de clavier, ni de souris !**

> Maintenant que vous avez déjà manipuler un peu de lignes de commandes dans la console du RPI et même que vous savez le faire depuis une console déportée sur votyre ordinateur de bureau, voici quelques *tips*:
>
> * avec les flèches ↑/↓ vous pouvez accéder à l'historique des commandes précédentes/suivantes. 
>
> * vous pouvez copier une ligne de commande puis la **coller dans la console via un clic droit**.
> * pour redémarrer proprement le RPI il suffit de taper la commande `sudo reboot`
> * pour éteindre le RPI taper la commande `sudo shutdown -h now`

#### Mettre à jour la date et l'heure malgré le proxy Inrap

Dans la console taper `sudo apt install htpdate`.

Une fois l'installation faite, taper `date`pour vérifier.

> <u>Note:</u> il est possible qu'il persiste un problème de fuseau horaire. Dans ce cas le modiçfier grâce à la commande `sudo raspi-config` puis `I2 Change Timezone` >  `Europe` > `Paris`



## Installer et paramétrer les logiciels sur le RPI

Pour cela il suffit de suivre la procédure d'instalation sur **Ubuntu** expliquée sur le site [docs.opendatakit.org](https://docs.opendatakit.org/aggregate-tomcat/)

### Installer un serveur Apache Tomcat ![logo_tomcat](images/logo_tomcat.png)

1. **Installer Java:** dans la console taper `sudo apt-get install openjdk-8-jre`.

Si la console vous invite à valider avec un message du genre ![validation](images/3_console_validation.png) valider en tapant `y` [Entrée]

2. Vérifier que  *"the installed Java bin directory is in the PATH environment variable"* !!! en tapant la commande `java -version`

   * si la réponse est **java: command not found** c'est qu'il y a un problème !

     > <u>Note:</u> cela m'est arrivé en installant la version OpenJDK 11 LTS pourtant conseillée dans la doc

   * sinon tout va bien (la commande renvoie la version openjdk) !          

3. **Installer le serveur Apache Tomcat 8.5**: avec la commande `sudo apt-get install tomcat8 tomcat8-common tomcat8-user tomcat8-admin` (accepter l'installation en tapant `y ` si nécessaire).

4. **Vous avez installé un serveur web sur le RPI !!** pour le vérifer ouvrez un explorateur internet et tapez l'adresse **IP_du_raspberry:8080**

=> Une page web devrait s'afficher avc un message encourageant !

![it works](images/4_it_works.png)

5. Configurer  l'accès distant au RPI
[tutoriel source (eng)]( http://www.vancura.cz/tutorial-build-full-java-server-with-tomcat8-at-public-ip-with-raspberry-pi/)

Pour ajouter un utilisateur à l'interface de gestion:
* on va stopper le serveur Tomcat: `sudo service tomcat8 stop`
* accéder au dossier /conf  (contenant le fichier de configuration): ` cd /var/lib/tomcat8/conf`
> <u>Note:</u> vous remarquez que l'invite de commande a changée: vous êtes dans le dossier **/var/lib/tomcat8/conf** ![console_cd](images/5_console_cd.png)
* éditer le fichier de configuration tomcat-user.xml : `sudo nano tomcat-users.xml` et 
* ajouter les 2 lignes suivantes après **<tomcat-users>**
```
<role rolename="manager-gui"/>
<user username="tomcat" password="miaou" roles="manager-gui"/>
```
![editeur nano](images/6_editeur_nano.png)

> ![Attention](images/attention.png) Dans l'éditeur tout se fait au clavier, comme dans le reste de la console. Pas de souris !
>
> Pour se déplacer dans l'éditeur utiliser les flèches ↑↓, pour sauter une ligne [Entrée] et pour copier un clic-droit.
>
> Pour sortir de l'éditeur nano et enregistrer les modifications faites **[Ctrl]+X** puis **Y** (N si vous ne voulez pas enregistrer)

* Redémarrer le serveur Tomcat avec la commande `sudo service tomcat8 start`

> <u>Note:</u> pour revenir à la racine du RPI taper la commande `cd`

![no](images/NO.png) Attention aux guillemets ! regarder la coloration syntaxique pour voir si l'éditeur a bien interprété la commande. Par exemple les identifiants et les guillemets doivent apparaitre en mauve.  j'ai mis *login*=<u>tomcat</u> et *password*=<u>miaou</u> 

**=> Pour vérifier le bon fonctionnement du bouzin, dans un explorateur web taper l'adresse *IP_du_raspberry:8080/manager/html***

Après avoir rentré vos identifiants (tomcat) et mot de passe (miaou) la page suivante devrait apparaître
![tomcat admin](images/7_tomcat_admin.png)



### Installer une BDD PostgreSQL![pg](images/logo_pg.png)

[tutoriel source (eng)](https://opensource.com/article/17/10/set-postgres-database-your-raspberry-pi)

<u>Note:</u> on utilise indifféremment PostgreSQL et postgres pour parler du [logiciel](https://www.postgresql.fr/)

#### Installation de PostgreSQL

On commence par installer les paquets (logiciel) de Postgres avec la commande
```
sudo apt install postgresql libpq-dev postgresql-client postgresql-client-common -y
```

> <u>Note:</u> copier-coller la totalité de la commande dans la Console du Pi et valider par `Y`si nécessaire. <u>attention</u> apparemment il faut bien mettre le `Y`en majuscule !

#### Configuration

On configure ensuite la Base de Données

1. D'abord on "bascule" sur l'utilisateur postgres: `sudo su postgres`

2. Puis on crée un utilisateur (on utilise l'utilisateur pi qui est celui par défaut du RPI en lui donnant des droits d'accès à la BDD postgreSQL): `createuser pi -P --interactive`

   On nous demande:

   * de renseigner le mot de passe, puis de le confirmer : j'ai utilisé le même que pour le PI c'est à dire **raspberry** . <u>attention</u> sous linux on ne voit pas se que l'on tape quand c'est mot de passe !
   
   * si l'utilisateur est un super-utilisateur : refuser en tapant `n`
   
   * si l'utilisateur à le droit de créer une base de données: accepter en tapant `y`
   
   * si l'utilisateur est autorisé a créer d'autres rôles (utilisateurs): accepter en tapant `y`
   
     ![create user](images/8_pg_createuser.png)

> <u>Note:</u> pour supprimer un utilisateur utiliser la commande `dropuser nom_utilisateur -i` ou pour plus de détail taper `dropuser  --help`

#### Créer sa première Base de Données

On va maintenant créer une BDD test en ligne de commande !!

1. On se connecte à PostgreSQL: `psql`

2. Puis on créer une BDD test (c'est son nom): `create database test;`

3. La BDD test créée on peut revenir à la console du PI (on sor t de postgres) en faisant 2 fois **[Ctrl+D]**

4. Dorénavant pour se connecter à notre BDD test il suffit dans la console du PI de taper `psql test` 

   => On accède alors à la Console PostgreSQL de notre BDD test, pour preuve la nouvelle invite de commande sous la forme ![pg_invite](images/9_pg_invite.png)

Une fois la BDD crééé on peut commencer à créer notre première table et y mettre des données !

1. On crée notre première table **people** avec 2 champs de type texte: un champ "nom" et un champ "company":
  `create table people (name text, company text);`
2. On peut maintenant rentrer des données dans la table **people**, toujours en ligne de commande:
`insert into people values ('Harry Cover', 'Inrap');`
Puis
`insert into people values ('Thomas Teu', 'Université');`

3. Enfin, on peut faire une requête SQL pour visualiser lae contenu de la table **people**:
```sql
select * from people;
```

> ![Attention](images/attention.png) **Lorsque vous êtes dans la console de postgres les commandes  (qui sont des requêtes SQL) finissent toujours par un point-virgule (;)**

![requêtes_SQL](images/10_pg_requetes.png)


### Se connecter à distance à votre BDD PostgreSQL

L'interêt d'une BDD PostgreSQL est justement d'être une BDD en mode serveur client c'est à dire que l'on dissocie **le serveur de Base de Données** (c'est tout se que l'on vient de faire jusque là !!) de la partie **client**. cette dernière peut utilmiser différents logiciel, il suffit de paramétrer la connexion au serveur postgre.

Nous voyons ici 2 possibilités complémentaires:

* se connecter depuis le logiciel QGIS (même si nos tables ne sont pas géométriques)
* installer une interface graphique d'administration : PGadmin

#### Authoriser les connexions au serveur PostgreSQL depuis l'extérieur

Dans un premier temps il va donc falloir authoriser les connexions à PostgreSQL depuis l'extérieur (oui Linux c'est comme ça.. sécurisé par défaut!):

1. Se déplacer dans le dossier **/etc/postgresql/9.6/main/**  en tapant la commande `cd etc/postgresql/11/main/`

   

>  ![Attention](images/attention.png) **<u>Attention</u> **selon la version de postgres installée le numéro peut changer pour cela:
> aller dans le répertoire /etc/postgresql/ en tapant `cd /etc/postgresql/`psql
> puis lister son contenu avec la commande `ls`
> la console renvoie un chiffre qui correspond au dossier qui porte le numéro de la version installée  de postgres (11 dans mon cas):
>
> *  revenir à la racine avec la commande `cd`
> * puis reporter ce [numéro] dans le chemin de dossier à atteindre à l'aide la commande  `cd /etc/postgresql/[numéro]/main/`



2. Editer le fichier de configuration **postgresql.conf** et décommenter (enlever le dièse **#**) devant la ligne listen-adress:
   * éditer le fichier: `sudo nano postgresql.conf`
   * descendre avec ↓ jusqu'à la ligne **#listen_adresses = 'localhost'** 
   * supprimer le **#** et remplacer **localhost** par **\*** entre les apostrophes.
   

  avant:  

![pg conf avant](images/11_pg_conf_avant.png)

 après:  

![pg conf après](images/12_pg_conf_apres.png)

   * enregistrer **[Ctrl]+O** et **[Entrée]** puis Quitter **[ctrl]+X**

3. Editer le fichier de configuration **pg_hba.conf** et changer les adresses IP par ** 0.0.0.0/0** et **::/0**

   * éditer le fichier: `sudo nano pg_hba.conf`

   * Ajouter les lignes suivantes :

     ```
     # Database administrative login by Unix domain socket
     local   all             postgres                                peer
     
     # TYPE  DATABASE        USER            ADDRESS                 METHOD
     
     # "local" is for Unix domain socket connections only
     local   all             all                                     peer
     # IPv4 local connections:
     host    all             all             0.0.0.0/0               md5
     # IPv6 local connections:
     host    all             all             ::/0                    md5
     ```

   * Redémarrer le service PostgreSQL: `sudo service postgresql restart`

#### Se connecter depuis QGIS à PostgreSQL : ![superQ](images/logo_superQ.png) 

Une façon simple de se connecter à la BDD que vous avez créer et d'utiliser QGIS 

* Il suffit d'ajouter une connexion PostGis![icon_qgis_pg](images/icon_qgis_pg.png)

* et de la paramétrer

  ![qgis_pg_connexion](images/15_qgis_pg_connexion.png)

=> Toute la BDD peut être "administrée" (création, modification et suppression de tables, requêtes SQL, créations de vues, etc..) depuis le gestionnaire de BD ![icon_qgis_bdd](images/icon_qgis_bdd.png) et les enregistrements directement dans les tables attributaires ! La classe, non ?!

Si cela ne vous suffit pas et que vous avez des besoins plus pointus en terme d'adminisrtation de BDD vous pouvez installer et utiliser un logiciel dédié: PGadmin... sinon passez directement à l'instalation d'ODK Aggregate.

#### Se connecter via une interface graphique d'administration: PGadmin

[PGadmin](https://www.pgadmin.org/) est une interface graphique pour l'administration dédiée aux BDD PostgreSQL.

> <u>Note:</u> Pas forcément utile mais bon à savoir:
> Pour l'installer sur le PI depuis la console du PI (faire un [Ctrl]+D si vous êtes encore dans votre BDD postgres...): `sudo apt install pgadmin3`

1. Installer PGadmin IV sur votre PC après l'avoir télechargé [(lien)](https://www.pgadmin.org/download/pgadmin-4-windows/)

2. Ouvrir PGadmin IV et créer une nouvelle connexion

3. Paramétrer la connexion avec l'adresse IP du RPI et les identifiants utilisateurs de postgres:

   ![pg_adminIII](images/13_pgadm_connexion.png)![pg_adminIV](images/14_pgadm_connexion.png)
><u>tips:</u> pour connaitre l'adresse IP du RPI on peut taper la commande `hostname -I`
><u>Note:</u> Pgadmin II = cliquer sur connexion / Pgadmin IV = créer un serveur.


### Installer et paramétrer ODK Aggregate ![odk](images/logo_odk.png)

Maintenant que tout est prêt ou presque il faut préparer et installer ODK Aggreagte (qui récupère et "compile" les données des formulaires d'ODK) afin qu'il fasse son taf et envoie les données dans une BDD PostGis.

La procédure peut paraître complexe mais on ne la fait qu'une fois (enfin...  si cela vous rassure un peu dites le vous !) 

####  Télecharger et paramétrer ODK Aggregate

C'est maintenant que vous allez regretter Linus et ses lignes de commandes !

Nous allons Installer un logiciel qui va permettre de créer les fichiers de paramétrage d'ODK Aggregate sous la forme de 3 fichiers :

* ODKaggregate.war
* create_db_and_user.sql
* README.html

Ensuite il suffira de copier ces fichiers sur le RPI, au bon endroit et de suivre un pas à pas pour finir la configuration sur le RPI ! On y va ?

1. Télecharger [ODK Aggregate](https://github.com/opendatakit/aggregate/releases/latest) en choisissant bien la version qui correspond à votre système d'exploitation. Si vous êtes sous windows le nom du fichier devrait être quelque chose du genre **ODK-Aggregate-v2.0.5-Windows.exe.zip**. Dézipper-le ! et installer le logiciel. 
2. créer un répertoire ODK sur le bureau.
3. Executer le logiciel
4. **Setup:** [Next>]
5. **License Agreement:** Cocher *I Accept the agreement* [Next>] 
6. **Select an output parent directory:** cliquer sur l'icone Parcourir ![parcourir](images/odk1_parcourir.png)et sélectionner le dossier ODK nouvellement créé. [Next>]
7. **Choose Platform:** PostgreSQL évidemment ! [Next>]
8. **Pre-installation Requirements - Upgrading bla bla:** on s'en balance ! [Next>]
9. **Pre-installation Requirements - Apache Tomcat:** on s'en balance aussi ! [Next>]
10. **Apache Tomcat SSLConfiguration:** [Next>]
11. **Apache Tomcat SSLConfiguration:** On a pas de SSl certificate alors ... [Next>] 
12. **Apache Tomcat port Configuration:** <u>On laisse le Port par défaut et on mais l'adresse IP du serveur</u> Tomcat (la même que celle du raspberry pi: **attention** si elle change il faudra refaire la manip !) [Next>] 
13. **Pre-installation database Requirements - postgreSQL:** Déjà fait... [Next>] 
14. **Database server settings:** D'après se qu'ils disent on ne change rien [Next>] 
15. **ODK Aggregate database authentification settings:**
    * *Database username:* <u>aggregate</u>
    * *Database password:* <u>odkpi</u>
    * *Retype password:* <u>odkpi</u>
16. **ODK Aggregate database authentification settings:**
    * *Database Name:* <u>aggregate_pi</u>
    * *Database Schema Name:* <u>aggregate</u>
17. **ODK Aggregate Instance Name:** <u>inrap_odk</u>
18. **Super-user ODK Aggregate Username:** puisque c'est super-user je mets <u>superuser</u>
19. **Ready to Configure ODK Aggregate:**  This is the END... [Next>] 
20. c'est lui qui fait le boulot puis une fois **Completing the ODK Agrregate Setup Wizard** [Finish] 

=> Au final sur le burau j'ai bien un dossier ODK Aggregate qui contient 3 fichiers:

![3fichiers](images/odk2_fichiers.png)

#### Transférer les paramètres d'ODK sur le RPI

Le plus simple pour transférer des fichiers via le réseau est d'utiliser un client FTP, nous utilisons ici un des plus répandus: [FileZilla Client](https://filezilla-project.org/download.php?type=client) que nous avons téléchargé et installé.

><u>Note</u> pour les Inrapiens le logiciel est installé par défaut sur les ordinateurs.
>
>![attention](images/attention.png)Il faut d'abord activer ouvrir le port 22, le plus simple étant d'installer VNC depuis `sudo raspi-config` >  `5 Interfacing Options` >  `P3 VNC` > `<YES>` > `<OK>`

Dans FilleZilla:

1. Connectez-vous au rpi grâce à la barre de connexion rapide.
2. Dans la fenêtre **Site local** (votre ordinateur) naviguez dans l'explorateur pour accéder au dossier ODKAggregate nouvellement créé sur le bureau. Les 3 fichiers de configuration doivent s'afficher dans la fenêtre en dessous (4)
3. Dans la fenêtre **Site distant** (le RPI) faire un clic-droit sur le dossier pi et créer (et accéder) un nouveau dossier **[telechargement]**. 
4. Sélectionner les 3 fichiers (fenêtre en bas à gauche) et les glisser dans le dossier [telechargement] (fenêtre en bas à droite)

![FileZilla](images/16_filezilla.png)

Dans la console du RPI:

On peut vérifier que les fichiers ont bien été transférés grâce aux commandes:

* `ls`qui permet de lister le contenu du dossier actuel (<u>tips</u> : les dossiers apparaissents en violet, les fichiers en blanc, les fichiers compréssés en rouge)

* `cd mon_dossier` pour accéder à [mon_dossier]

  ![console](images/17_console.png)

* `cd` seul permet de revenir à la racine

Comme expliqué sur le site d'[ODK](https://docs.opendatakit.org/aggregate-tomcat/), une fois les fichiers transférés iol suffit de lire le fichier **README.html** créé par le configurateur ! Il faut donc ouvrir ce fichier sur son ordinateur (un double clic devrait l'ouvrir dans votre explorateur internet défini par défaut )

> <u>Note:</u> le fichier README.html explique pas à pas la procédure mais n'est pas dédié a une installation sur le RPI. Il va donc falloir faire quelques modifications...

Pour compléter l'installation, vous devez:

1. <u>Executer la console PostgreSQL en ligne de commande</u>: 

   * Vous devez être dans le dossier contenant les 3 fichiers sinon faites `cd telechargements`=> ![console](images/18_console.png)
   * `sudo su postgres` => le prompt (l'invite de commande) change => ![console](images/19_console.png)
   * `psql` => vous êtes dans postgres (dans la base de données par défaut: postgres et avec les identifiants par défaut: les même que pi), l'invite de commande a encore changé => ![console](images/19_console.png)
   * `\i create_db_and_user.sql`=> il crée la BDD **aggregate_pi** et l'utilisateur **postgres** => ![console](images/20_console.png)
   * *well done !* on peut quitter avec `\q`
   
2. <u>Copier le fichier **ODKAggregate.war** dans le dossier **/webapps** de l'instalation du serveur Apache Tomcat</u>: 

   * sortez de postgre avec un **[Ctrl]+d** =>  ![console](images/18_console.png)

   * en ligne de commande cela donne `sudo cp ODKAggregate.war /var/lib/tomcat8/webapps`

     > Sans rentrer trop dans les détails décortiquons cette commande:
     >
     > * `sudo` = lancer une commande en tant qu'administrateur
     > * `cp` = copier on peut pas vraiment faire plus simple
     > * `ODKaggregate.war`= nom du fichier à copier (il n'est pas préceder du chemin car on est déjà dans le dossier ou il se trouve)
     > * un espace
     > * `/var/lib/tomcat8/webapps` = le dossier de destination

   * On pourrait d'ailleurs vérifier que la copie a bien été faite avec `cd /var/lib/tomcat8/webapps` puis `ls`pour vérifier le contenu du dossier **/webapps**

   3. Démarrer Apache: a priori pas besoin c'est déjà fait.

   4. Quand il a finit son boulot, il ne vous reste plus qu'à vous rendre à l'adresse http://adresse_IP:8080/ODKAggregate

      => Vous êtes sur la page d'adinistration d'ODKAggregate ! il ne reste plus qu'à vous logger:

      * clic sur le lien  [Log in] en haut à droite de la fenêtre.
      * rentrer les identifiants <u>superuser</u> avec le mot de passe <u>aggregate</u>

      ![ODK_aggregate](images/21_ODKaggregate.png)

### Travailler avec un formulaire ODK

#### Préparer le formulaire

Cette rubrique fait l'objet d'un tutoriel à part... en attendant voir [ici par exemple (eng)](http://xlsform.org/en/)

#### Charger le formulaire dans ODKAggregate

Pour verser un formulaire ODK sous la forme **formulaire.xml** il suffit de passer par la page d'administration d'ODKAggregate à l'adresse  http://adresse_IP:8080/ODKAggregate.

Dans l'onglet **Form Management** > **Add a new form **:

* *Form definition (xml file):* cliquer le bouton [Parcourir] et charger le fichier **.xml**
* *Optional Media file(s):* cliquer le bouton [Parcourir] et charger le(s) fichier(s) images appellés par le formulaire (pour servir d'images à cliquer par ex.)
* Cliquer sur le bouton [Upload Form] pour charger le tout dans ODKAggregate.

#### Configurer le serveur sur l'application ODKcollect (Android)

Dans l'application [ODK collect](https://play.google.com/store/apps/details?id=org.odk.collect.android&hl=fr) préalablement télechargée et installé sur votre ordiphone (télephone ou tablette android) cliquez sur les 3 petits points en haut )à droite > Paramètres adminisrtateur > modifier les paramètres > Serveur

[retour à la page d'Accueil](README.md)
