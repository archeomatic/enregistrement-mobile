# Tentative d'installation et paramètrage du Raspberry Pi comme hotspot wifi



On essaye d'abord en s'inspirant de cette page au titre prometteur : [Créer un hotspot Wi-Fi en moins de 10 minutes avec la Raspberry Pi !](https://raspberry-pi.fr/creer-un-hotspot-wi-fi-en-moins-de-10-minutes-avec-la-raspberry-pi/)

### S'assurer que la connexion wifi du RPI reste disponible pour la création du hotspot

Attention une partie de ce qui suit est du pur Copier-Coller/plagiat dans ce ca le texte est mis en forme comme ci-dessous:

> Ceci fait, nous allons nous assurer que la connexion Wi-Fi de la  raspberry reste disponible pour la création du hotspot. Dans ce but,  nous allons simplement créer une copie du fichier de configuration  permettant la connexion à une box, afin de le garder de côté, et  modifier le fichier principal.
>  Pour cela, accédez au terminal de la raspberry pi et lancez les commandes suivantes :

```
sudo cp /etc/wpa_supplicant/wpa_supplicant.conf /etc/wpa_supplicant/wpa_supplicant.conf.sav
sudo cp /dev/null /etc/wpa_supplicant/wpa_supplicant.conf
```

> Enfin, éditez dans le fichier `/etc/wpa_supplicant/wpa_supplicant.conf` 

(Par contre là c'est de moi....)

Pour se faire aller dans le répertoire ainsi `cd /etc/wpa_supplicant/`
Puis lister le contenu du dosiier avec `ls`
et éditer le fichier avec l'éditeur nano: `sudo nano wpa_supplicant.conf`

> et ajoutez les lignes suivantes :

En même temps le fichier est vide je ne suis pas sûr que c'est bon !
```
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
```
**J'avoue.. je ne comprends pas bien se que je fais jusque là.. et je ne sais pas si c'est utile, ou si on ne pourrait pas passer directement à la suite...**

### Installation des logiciels

> Dans ce tutoriel, nous allons utiliser le logiciel hostapd, qui permet  de fabriquer un point d’accès avec la raspberry pi. Et pour faciliter  l’installation et l’administration de ce point d’accès, nous allons  également installer une interface web qui nous permettra de contrôler  notre hotspot, RaspAP (pour plus d’infos, allez lire la page Github du  logiciel, https://github.com/billz/raspap-webgui).

> Et la bonne nouvelle, c’est que pour faire tout ça nous n’aurons besoin de lancer qu’une seule commande :

```
wget -q https://git.io/voEUQ -O /tmp/raspap && bash /tmp/raspap
```

> Une fois la commande lancée, vous n’avez plus qu’à répondre aux quelques  questions qui vous seront posées, et l’installation de tous les  composants va se faire toute seule.

Après quelques acceptations, c'est à dire `y`+ [Entrée], le RPi redémarre...

Si on recherche les réseaux wifi le réseau **raspi-webgui** apparait.

Reste à s'identifier pour se connecter au réseu wifi du RPI:

* *utilisateur*: **raspi-webgui**
*  *mot de passe*: **ChangeMe**

> Vous pouvez modifier ce mot de passe, le nom du réseau, et bien d’autres choses en vous connectant à l’interface d’administration de votre  hotspot raspberry via votre navigateur internet, par défaut l’adresse  devrait-être `10.3.141.1`.

L'adresse IP du RPI est alors http://10.3.141.1 on accéde alors à l'interface d'administration du hotspot :

* *utilisateur*: **admin**
* *mot de passe*: **secret**

### Résultats partiels

La nouvelle adresse du RPI est bien 10.3.141.1

On peut se connecter au RPI **même s'il n'est branché sur aucun réseau !!** (en SSH par exemple) avec les identifiants du RPI  (pi+raspberry)

On peut donc se connecter à la **BDD postgres** (via QGIS par exemple) en mettant cette adresse IP et les identifiants du RPI (pi+raspberry)

On peut se connecter à la page de test d'ODKAggregate http://10.3.141.1:8080/ => It works ! (c'est pas moi qui le dit c'est la page)

Par contre, il fallait s'y attendre, on ne peut pas se connecter à la page http://10.3.141.1:8080/ODKAggregate/ qui nous renvoie à la page configurée avec le logiciel de configuration.. rien de plus normal et c'est la prochaine étape ...
