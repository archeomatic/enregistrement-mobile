# Projet Enregistrement mobile 2018-2019

## Présentation

Ce projet de recherche et développement souhaite s’articuler sur deux années consécutives et implique surtout deux agents de
l’Inrap, B. Houdusse et S. Badey. Il s’inscrit dans une démarche globale d’évolution de nos pratiques quotidiennes en archéologie
vers une utilisation accrue des outils numériques.
Il vise principalement à fournir une solution complémentaire – et moins chère – à l’utilisation actuelle des PC durcis dit « tablettes »,
afin de pouvoir généraliser ce type d’enregistrement de terrain mobile à une large gamme d’opérations.

## Objectifs

Les résultats attendus sont :
- dans un premier temps, la création de multiples formulaires d’enregistrement « standards » qui peuvent répondre à diverses situations : enregistrement stratigraphique, renseignement des inventaires mobiliers ou de documentations (minutes, photos…)
- dans un second temps, mise à l’épreuve de ces formulaires en situation réelle, en les utilisant dans l’application [OpenDataKit](https://docs.opendatakit.org/).
- dans un troisième temps : travail sur l’intégration des enregistrements sur un serveur de base de données. Dans cette optique, les
agents du projet souhaitent solliciter l’aide d’agents de la DSI afin de mettre en place facilement un (ou des) serveurs nécessaires, à
l’aide notamment de nano-ordinateurs [RaspberryPi 3](https://www.raspberrypi.org/)

## Gitlab

Ce GitLab est un dépôt pour la documentation que nous avons produit pendant le projet: traduction de support de documentation, aggrégation de diverses ressources,...

Vous trouverez:
- [La traduction de la documentation détaillée de création de formulaires au format XLSForm](XLSForm/XLSForm.md)
- [Un pas à pas d'installation/configuration/paramétrage de Tomcat/Postgre/ODKAggregate sur un raspberry pi](Raspberry/Install_Tomcat_PG_ODK_onRPI.md)
- [un retour d'expérience sur le terrain par M. Heppe](Retex/2019_magH_Bilan_un_an.pdf)
- [Un BROUILLON de pas à pas pour créer un hotspot wifi à partir du RPI](Raspberry/Install_RPI_Hotspot.md)