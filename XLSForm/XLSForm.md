[retour à la page d'Accueil](README.md)

#  Qu'est-ce qu'un XLSForm ?

XLSForm est un formulaire standard créé pour simplifier la création de formulaires dans Excel. L'écriture se fait dans un format lisible par l'homme à l'aide d'un outil familier que presque tout le monde connaît - Excel. XLSForms fournit un standard pratique pour le partage et la collaboration sur les formulaires de création. Ils sont simples à utiliser, et permettent l'écriture de formulaires complexes par quelqu'un qui connaît bien la syntaxe décrite ci-dessous.

Le XLSForm est ensuite converti en un XForm, un standard de formulaire ouvert populaire, qui vous permet de créer un formulaire avec des fonctionnalités complexes comme les branchements conditionnels d'une manière cohérente à travers un certain nombre de plates-formes de collecte de données web et mobiles. XLSForms est compatible avec le sous-ensemble de fonctionnalités XForm supportées par [Javarosa Project](https://bitbucket.org/javarosa/javarosa/wiki/Home). XLSForms est supporté par un certain nombre de plates-formes de collecte de données populaires.

## Format de base

Chaque classeur Excel comporte habituellement deux feuilles de travail : **survey** (enquête) et **choices** (choix). Une troisième feuille de travail facultative appelée **settings** (paramètres) permet d'ajouter des spécifications supplémentaires à votre formulaire et est décrite ci-dessous.

### La feuille de travail "survey"

Cette feuille de travail donne à votre formulaire sa structure générale et contient la plus grande partie du contenu du formulaire. Il contient la liste complète des questions et des informations sur la façon dont elles doivent apparaître dans le formulaire. Chaque ligne représente habituellement une question ; cependant, il y a certaines autres caractéristiques décrites ci-dessous que vous pouvez ajouter au formulaire pour améliorer l'expérience de l'utilisateur.

### La feuille de travail "choices"

Cette feuille de travail est utilisée pour spécifier les choix de réponse aux questions à choix multiples. Chaque ligne représente un choix de réponse. Les choix de réponse portant le même **nom de liste** sont considérés comme faisant partie d'un ensemble de choix connexes et apparaîtront ensemble pour une question. Cela permet également de réutiliser un ensemble de choix pour des questions multiples (*par exemple, oui/non*).

Ces deux feuilles de travail ont un ensemble de colonnes obligatoires qui doivent être présentes pour que le formulaire fonctionne. De plus, chaque feuille de travail comporte un ensemble de colonnes facultatives qui permettent de contrôler davantage le comportement de chaque entrée du formulaire, mais qui ne sont pas essentielles. Chaque entrée doit avoir des valeurs pour chacune des colonnes obligatoires, mais les colonnes facultatives peuvent être laissées en blanc.

* La feuille de travail de l'enquête **survey** comporte trois colonnes obligatoires : **type**, **name** et **label**.

  * La colonne **type** spécifie le type d'entrée que vous ajoutez.

  * La colonne **name** spécifie le nom unique de la variable pour cette entrée. Deux entrées ne peuvent pas avoir le même nom.

  * La colonne **label** (étiquette) contient le texte réel que vous voyez dans le formulaire. Il est également possible d'utiliser des [colonnes de traduction d'étiquettes][language].

    ​

| type                | name     | label                |
| ------------------- | -------- | -------------------- |
| today               | today    |                      |
| select_one gender   | gender   | Respondent's gender? |
| integer             | age      | Respondent's age?    |
| =================== | ======== | ==================== |
| survey              |          |                      |

* La feuille de travail **choices** comporte également 3 colonnes obligatoires : **list name**, **name** et **label**.

  * La colonne **list name** vous permet de regrouper un ensemble de choix de réponses connexes, c'est-à-dire des choix de réponses qui devraient apparaître ensemble sous une question.

  * La colonne **name** spécifie le nom de la variable unique pour ce choix de réponse.

  * La colonne **label** indique le choix de réponse exactement comme vous voulez qu'il apparaisse sur le formulaire. Il est également possible d'utiliser des [colonnes de traduction d'étiquettes][language].


| list_name           | name        | label                |
| ------------------- | ----------- | -------------------- |
| gender              | transgender | Transgender          |
| gender              | female      | Female               |
| gender              | male        | Male                 |
| gender              | other       | Other                |
| =================== | ========    | ==================== |
| choices             |             |                      |

Les colonnes que vous ajoutez à votre classeur Excel, qu'elles soient obligatoires ou facultatives, peuvent apparaître dans n'importe quel ordre. Les colonnes facultatives peuvent être omises complètement. N'importe quel nombre de lignes peut être laissé en blanc. Tous les formats de fichiers.xls sont ignorés, vous pouvez donc utiliser des lignes de division, des ombres et d'autres formats de police pour rendre le formulaire plus lisible.

Une chose à garder à l'esprit lors de la création de formulaires dans Excel est que la syntaxe que vous utilisez doit être précise. Par exemple, si vous écrivez**Choices** ou **choice** au lieu de **choices** , le formulaire ne fonctionnera pas.

## Types de questions

XLSForm supporte un certain nombre de types de questions. Ce ne sont que quelques-unes des options que vous pouvez saisir dans la colonne **type** de la feuille de travail **survey** dans votre XLSForm :

| Question type             | Answer input (type de saisie attendue)   |
| ------------------------- | ---------------------------------------- |
| integer                   | Entier (cad nombre entier) input.        |
| decimal                   | Nombre décimal.                          |
| range                     | [Intervalle](range).                     |
| text                      | Texte libre.                             |
| select_one [options]      | [Question à choix multiples](multiple-choice) question; une seule réponse peut être sélectionnée. |
| select_multiple [options] | [Question à choix multiples](multiple-choice) question; plusieurs réponses peuvent être sélectionnées. |
| note                      | Affiche une note à l'écran (ne prend aucune entrée). |
| geopoint                  | Collecte une [simple coordonée GPS](gps). |
| geotrace                  | Enregistre une ligne de deux (ou plus) coordonées GPS. |
| geoshape                  | Enregistre un polygone à partir de plusieurs coordonées GPS; le dernier point est le même que le premier point. |
| date                      | Date.                                    |
| time                      | Heure.                                   |
| dateTime                  | Accepte en entrée date et heure.         |
| image                     | Prend une photo ou charge une image existante. |
| audio                     | Enregistrement audio ou charge un fichier audio existant. |
| video                     | Enregistre une video ou charge une vidéo existante. |
| file                      | Fichier générique (txt, pdf, xls, xlsx, doc, docx, rtf, zip) |
| barcode                   | Scan un code-barre, l'appli  *barcode scanner* doit nécessairement être installée. |
| calculate                 | Effectuer un calcul ; voir la section **Calcul** ci-dessous. |
| acknowledge               | Une invite de confirmation qui définit la valeur sur "OK" si elle est sélectionnée. |
| hidden                    | Un champ sans **UI** (élément interface utilisateur) associé. |
| xml-external              | Ajoute une référence à un [fichier de données XML](xml-external) externe. |

### GPS

Par exemple, pour recueillir le nom et les coordonnées GPS d'un magasin, vous écrivez ce qui suit :

| type       | name         | label                                    |
| ---------- | ------------ | ---------------------------------------- |
| text       | store_name   | What is the name of this store?          |
| geopoint   | store_gps    | Collect the GPS coordinates of this store. |
| ========== | ============ | ============================================ |
| survey     |              |                                          |

Voir le formulaire d'exemple [question_types XLSForm](https://docs.google.com/spreadsheets/d/1P4roHU0iC_Xx0028oKK656FvH4MBWecIw-HJ7JRwrYs/edit?usp=sharing) pour un aperçu de chaque type de question utilisé dans un formulaire.

### GPS avec seuil de précision

Lors de l'enregistrement des coordonnées GPS dans ODK Collect, ODK collect collecte automatiquement les  coordonnées gps lorsqu'un niveau de précision de 5 mètres ou moins est atteint. Vous pouvez modifier ce comportement par défaut en spécifiant un **Seuil de Précision**, qui peut être inférieur à 5m ou supérieur à 5m. Vous devrez ajouter une colonne avec en entête **body::accuracyThreshold** sur la feuille **survey **de votre XLSForm. Indiquez ensuite votre valeur seuil de précision préférée pour cette colonne sur votre question de type **geopoint**, comme dans l'exemple ci-dessous :

| type       | name         | label                                    | body::accuracyThreshold   |
| ---------- | ------------ | ---------------------------------------- | ------------------------- |
| geopoint   | store_gps    | Collect the GPS coordinates of this store. | 1.5                       |
| ========== | ============ | =========================================== | ========================= |
| survey     |              |                                          |                           |

Voir le formulaire d'exemple [ gps_accuracy_threshold](https://docs.google.com/spreadsheets/d/1kdV-UF65WONU251Zh7ngdPiQ_VrEKTNmgOHyNonSsGw/edit?usp=sharing) pour un exemple utilisant cet attribut. 

### Questions à choix multiples

XLSForm prend en charge les questions **select_one** (sélectionner une seule réponse) et **select_multiple** (sélectionner plusieurs réponses). La rédaction d'une question à choix multiples nécessite l'ajout d'une feuille **choices** à votre classeur Excel. Voici un exemple d'une question **select_one** :

| type              | name          | label                     |
| ----------------- | ------------- | ------------------------- |
| select_one yes_no | likes_pizza   | Do you like pizza?        |
| ============      | ============= | ========================= |
| survey            |               |                           |

<p/>

| list name    | name        | label    |
| ------------ | ----------- | -------- |
| yes_no       | yes         | Yes      |
| yes_no       | no          | No       |
| ============ | =========== | ======== |
| choices      |             |          |

Notez que le **yes_no** dans la feuille de travail **survey** doit correspondre au **yes_no** dans la colonne **list_name** dans la feuille de travail **choices**. Cela permet de s'assurer que le formulaire affiche la bonne liste de choix de réponses pour une question particulière.

Nous pouvons également ajouter des questions à choix multiples qui permettent de sélectionner plusieurs réponses, comme ceci :

| type                           | name              | label                                    |
| ------------------------------ | ----------------- | ---------------------------------------- |
| select_multiple pizza_toppings | favorite_toppings | What are your favorite pizza toppings?   |
| ===========================    | ============      | =========================================== |
| survey                         |                   |                                          |

<p/>

| list name      | name          | label                     |
| -------------- | ------------- | ------------------------- |
| pizza_toppings | cheese        | Cheese                    |
| pizza_toppings | pepperoni     | Pepperoni                 |
| pizza_toppings | sausage       | Sausage                   |
| ============   | ============= | ========================= |
| choices        |               |                           |

### Précisez "autres"

Pour les questions à choix multiples, les enquêtes comprennent souvent une option avec une case à cocher **autre**  lorsque leur choix de réponse n'est pas listé. Ensuite, on leur demande généralement de spécifier l'autre option. Ceci est possible grâce à XLSForm en incluant **or_other** après le nom de la liste de choix de réponse (séparé par un espace)  dans la feuille de calcul **survey**. La feuille **choices**  reste la même. Voir ci-dessous :

| type                                    | name             | label                                    |
| --------------------------------------- | ---------------- | ---------------------------------------- |
| select_multiple pizza_toppings or_other | favorite_topping | What are your favorite pizza toppings?   |
| ===========================             | ============     | =========================================== |
| survey                                  |                  |                                          |

<p/>

| list name      | name       | label                     |
| -------------- | ---------- | ------------------------- |
| list name      | name       | label                     |
| pizza_toppings | cheese     | Cheese                    |
| pizza_toppings | pepperoni  | Pepperoni                 |
| pizza_toppings | sausage    | Sausage                   |
| ============   | ========== | ========================= |
| choices        |            |                           |

Cliquez sur le lien pour voir la totalité du  [pizza_questionnaire](https://docs.google.com/spreadsheets/d/1y9LcFUaJ_MDRpqbzHVxkD_k6YzSQCllqh3Excy4iffg/edit?usp=sharing).

**Mise en garde**
Lorsque vous exportez des données en utilisant cette option **or_other**, dans la colonne **favorite_topping**, vous verrez une valeur **autre**. Une colonne séparée contiendra la réponse aux questions pour lesquelles l'utilisateur en a sélectionné d'autres. Cela rend l'analyse des données plus lourde, c'est pourquoi nous ne recommandons pas le concept **or_other** pour les efforts de collecte de données à grande échelle. Voir la section **Relevant** (pertinence) ci-dessous pour une méthode alternative plus appropriée pour les projets à grande échelle.

### Range
Pour restreindre les entrées de type chiffre entier à une plage spécifique,vous pouvez utiliser la question de type  **range** (plage). Cette question peut être utilisée avec 3 paramètres optionnels séparés par des espaces:  **start** (début), **end** (fin), and **step** (pas) dans une colonne  **parameters**. Les valeurs par défaut sont respectivement  0, 10, et 1.. L'exemple ci-dessous va créer une question qui permet d'entrer un chiffre entier  de 0 à 17 avec un pas de 1.

| type                      | name         | label                         | parameters            |
| ------------------------- | ------------ | ----------------------------- | --------------------- |
| range                     | amount       | What is the age of the child? | start=0 end=17 step=1 |
| ========================= | ============ | ============================= | ==================    |
| survey                    |              |                               |                       |

### Metadata

XLSForm dispose d'un certain nombre d'options de type de données pour la collecte des métadonnées :

| Metadata type | Meaning                                  |
| ------------- | ---------------------------------------- |
| start         | Date et heure de début de l'enquête.     |
| end           | Date et heure de fin de l'enquête.       |
| today         | jour de l'enquête.                       |
| deviceid      | IMEI (International Mobile Equipment Identity) |
| subscriberid  | IMSI (International Mobile Subscriber Identity) |
| simserial     | SIM serial number.                       |
| phonenumber   | Numéro de télephone (si disponible).     |
| username      | Nom d'utilisateur configuré (si disponible). |
| email         | Adresse e-mail configurée (si disponible). |

Notez que certains champs de métadonnées ne s'appliquent qu'aux formulaires basés sur les téléphones mobiles.

Si je voulais que mon enquête recueille toutes ces métadonnées, je mettrais ce qui suit au début de l'enquête (feuille **survey**) :

| type                      | name         | label |
| ------------------------- | ------------ | ----- |
| start                     | start        |       |
| end                       | end          |       |
| today                     | today        |       |
| deviceid                  | deviceid     |       |
| subscriberid              | subscriberid |       |
| simserial                 | simserial    |       |
| phonenumber               | phonenumber  |       |
| username                  | username     |       |
| email                     | email        |       |
| ========================= | ============ | ===   |
| survey                    |              |       |

Notez qu'il n'y a pas d'étiquettes associées aux questions de type métadonnées.  C'est parce que le téléphone capture automatiquement ces variables. Ces questions n'apparaîtront pas à l'écran du téléphone, mais vous les verrez lorsque vous consulterez les données de votre sondage.
Le [Tutoriel XLSForm](https://docs.google.com/spreadsheets/d/1OPBXLH8XAVPfyOjoC4-gn2bhZ4hOm2gCtIEyszw0NRo/edit?usp=sharing) montre comment les métadonnées sont utilisées dans un formulaire.

### External XML data

#### Données XML externes

Pour les utilisateurs avancés, qui doivent effectuer des requêtes complexes sur des données externes sans restrictions, un fichier de données XML externe peut être ajouté avec le type de question **xml-external**. La valeur de la colonne **name** peut être utilisée pour faire référence à ces données dans n'importe quelle formule (par exemple pour un calcul (*calculation*), une contrainte (*constraint*) , une pertinence (*relevant*) ou un *choice_filter*) en utilisant la fonction **instance('name')**. Un fichier portant le même nom et l'extension **.xml**  doit être téléchargé avec le formulaire. Voir ci-dessous pour un exemple qui nécessite le téléchargement d'un fichier appelé houses.xml avec le formulaire.

| type                      | name         | label           | calculation                              |
| ------------------------- | ------------ | --------------- | ---------------------------------------- |
| xml-external              | houses       |                 |                                          |
| integer                   | rooms        | How many rooms? |                                          |
| calculate                 | count        |                 | count(instance('houses')/house[rooms = current()/../rooms ]) |
| ========================= | ============ | ==========      | ============================================================ |
| survey                    |              |                 |                                          |

## Conseils (*hints*)

### Conseils courants

Parfois, vous voulez ajouter un petit conseil à l'utilisateur à une questionde votre formulaire, en lui indiquantcomment répondre à la question, mais vous ne voulez pas quecette aide fasse partie de la question elle-même. Il est facile d'ajouter des conseils aux questions dans XLSForms.  Ajoutez simplement une colonne **hint** et ajoutez votre message d'aide. Voir ci-dessous pour un exemple.

| type     | name     | label                                    | hint                                     |
| -------- | -------- | ---------------------------------------- | ---------------------------------------- |
| text     | name     | What is the name of this store?          | Look on the signboard if the store has a signboard. |
| geopoint | geopoint | Collect the GPS coordinates of this store. |                                          |
| ======== | ======== | ========================================== | ==================================================== |
| survey   |          |                                          |                                          |
Le[Tutoriel XLSForm](https://docs.google.com/spreadsheets/d/1OPBXLH8XAVPfyOjoC4-gn2bhZ4hOm2gCtIEyszw0NRo/edit?usp=sharing) fournit d'autres exemples de questions avec des astuces.

### Conseils cachés

Il y a un indice particulier qui n'est normalement pas affiché dans le formulaire. Il n'est affiché que dans des vues spéciales. Un exemple consisterait à montrer ces conseils sur des imprimés ou lors d'une formation pour les recenseurs. Ces astuces sont appelées _guidance hints_ et peuvent être ajoutées dans la colonne **guidance_hint**. Voir ci-dessous pour un exemple. 

| type     | name     | label      | guidance_hint                         | relevant    |
| -------- | -------- | ---------- | ------------------------------------- | ----------- |
| integer  | age      | Age?       |                                       |             |
| text     | name     | Name?      | This will only be shown for age > 18. | ${age} > 18 |
| ======== | ======== | ========== | ===================================== | =========== |
| survey   |          |            |                                       |             |

## Constraint (Contraintes)

Une façon d'assurer la qualité des données est d'ajouter des contraintes aux champs de données de votre formulaire.  Par exemple, lorsque vous demandez l'âge d'une personne, vous voulez éviter les réponses impossibles, comme -22 ou 200.  Ajouter des contraintes de données dans votre formulaire est facile à faire.  Il vous suffit d'ajouter une nouvelle colonne, appelée **constraint**, et de taper la formule précisant les limites de la réponse.  Dans l'exemple ci-dessous, la réponse pour l'âge de la personne doit être inférieure ou égale à 150. Notez comment le ``.``  dans la formule se réfère à la variable question.

| type     | name     | label            | constraint |
| -------- | -------- | ---------------- | ---------- |
| integer  | age      | How old are you? | . <= 150   |
| ======== | ======== | ==============   | =========  |
| survey   |          |                  |            |

Dans cet exemple, la formule ``. <= 150`` signifie que la valeur entrée ``.`` pour la question doit être inférieure ou égale à 150. Si l'utilisateur met 151 ou plus comme réponse, il ne sera pas autorisé à passer à la question suivante ou à soumettre le formulaire.

D'autres expressions utiles à utiliser dans la colonne **constraint** peuvent être trouvées[ici] (http://opendatakit.org/help/form-design/binding/). Consultez la section **Operators**.

### Message de contrainte

Si vous voulez inclure un message avec votre contrainte, indiquant à l'utilisateur pourquoi la réponse n'est pas acceptée, vous pouvez ajouter une colonne **constraint_message** à votre formulaire.  Voir l'exemple ci-dessous.

| type     | name           | label            | constraint | constraint_message                       |
| -------- | -------------- | ---------------- | ---------- | ---------------------------------------- |
| integer  | respondent_age | Respondent's age | . >=18     | Respondent must be 18 or older to complete the survey. |
| ======== | ========       | ===============  | =========  | =============================            |
| survey   |                |                  |            |                                          |
Dans cet exemple, si l'utilisateur entre un âge inférieur à 18 ans, le message d'erreur dans la colonne **constraint_message** apparaît. D'autres exemples de contraintes ont été illustrés dans ce[XLSForm](https://docs.google.com/spreadsheets/d/1g12xGrOsnNYezG6WtTfeusxzypRT1JHeUC2uNbe03sc/edit?usp=sharing).

## Relevant (Pertinence)

Une grande caractéristique de XLSForm est la possibilité de sauter une question ou de faire apparaître une question supplémentaire en fonction de la réponse à une question précédente. Vous trouverez ci-dessous un exemple de la façon de procéder en ajoutant une colonne **relevant** pour une question **select_one**, en utilisant notre exemple de garniture de pizza (*pizza topping*):


| type                                    | name             | label              | relevant                                 |
| --------------------------------------- | ---------------- | ------------------ | ---------------------------------------- |
| select_one yes_no                       | likes_pizza      | Do you like pizza? |                                          |
| select_multiple pizza_toppings or_other | favorite_topping | Favorite toppings  | ${likes_pizza} = 'yes'                   |
| ========                                | ========         | =================  | ==================================================== |
| survey                                  |                  |                    |                                          |

Dans cet exemple, on demande à la personne interrogée : "Vous aimez la pizza ?" Si la réponse est **oui**, la question sur la garniture de pizza apparaît ci-dessous. Notez le ``${ }`` autour de la variable **likes_pizza**.  Ceux-ci sont nécessaires pour que le formulaire puisse faire référence à la variable de la question précédente.  

Dans l'exemple ci-dessous, nous utilisons une syntaxe de pertinence pour une question **select_multiple**, qui est légèrement différente de l'exemple **select_one** ci-dessus.

| type                                    | name             | label                                 | relevant                                |
| --------------------------------------- | ---------------- | ------------------------------------- | --------------------------------------- |
| select_one yes_no                       | likes_pizza      | Do you like pizza?                    |                                         |
| select_multiple pizza_toppings or_other | favorite_topping | Favorite toppings                     | ${likes_pizza} = 'yes'                  |
| text                                    | favorite_cheese  | What is your favorite type of cheese? | selected(${favorite_topping}, 'cheese') |
| ======================================= | ================ | ===================================== | ===================                     |
| survey                                  |                  |                                       |                                         |

<p/>

| list name      | name         | label     |
| -------------- | ------------ | --------- |
| pizza_toppings | cheese       | Cheese    |
| pizza_toppings | pepperoni    | Pepperoni |
| pizza_toppings | sausage      | Sausage   |
| ============== | ============ | ========= |
| choices        |              |           |

Puisque la question sur la garniture de pizza (*pizza_toppings*) autorise des réponses multiples, nous devons utiliser l'expression ``selected(${favorite_topping}, 'cheese')``, parce que nous voulons que la question sur le fromage apparaisse chaque fois que l'utilisateur sélectionne **cheese** comme une des réponses (que des réponses supplémentaires soient sélectionnées ou pas).

Plus tôt, nous avons mentionné qu'il existait une autre méthode pour spécifier une autre méthode pour les questions à choix multiples, qui est plus appropriée pour les enquêtes à grande échelle. Ceci peut être fait en utilisant la même syntaxe que dans l'exemple ci-dessus :

| type                           | name                    | label                                  | relevant                                |
| ------------------------------ | ----------------------- | -------------------------------------- | --------------------------------------- |
| select_multiple pizza_toppings | favorite_toppings       | What are your favorite pizza toppings? |                                         |
| text                           | favorite_toppings_other | Specify other:                         | selected(${favorite_toppings}, 'other') |
| =============================  | ==================      | =====================================  | ======================================= |
| survey                         |                         |                                        |                                         |

<p/>

| list name           | name       | label               |
| ------------------- | ---------- | ------------------- |
| pizza_toppings      | cheese     | Cheese              |
| pizza_toppings      | pepperoni  | Pepperoni           |
| pizza_toppings      | sausage    | Sausage             |
| pizza_toppings      | other      | Other               |
| =================== | ========== | =================== |
| choices             |            |                     |

Notez que vous devez inclure **other** comme choix de réponse dans la feuille de calcul **choices**.

## Formules

Les formules sont utilisées dans les colonnes **constraint** (contrainte), **relevant** (pertinence) et **calculation** (calcul). Vous avez déjà vu quelques exemples dans les sections **Contraintes** et **Rapports** ci-dessus.  Les formules vous permettent d'ajouter des fonctionnalités supplémentaires et des mesures de la qualité des données à vos formulaires.  

Les formules sont composées de fonctions et d'opérateurs (+,*,div,etc.). Une liste complète et bien documentée des opérateurs et des fonctions se trouve dans la [documentation ODK](https://docs.opendatakit.org/form-operators-functions/). Pour la spécification technique,  voir aussi les [fonctions supportées](https://opendatakit.github.io/xforms-spec/#xpath-functions).

## Calculation (Calcul)

Votre enquête peut effectuer des calculs en utilisant les valeurs des questions précédentes. Dans la plupart des cas, il faudra pour cela insérer une question **calculate**. Par exemple, dans l'enquête ci-dessous, nous avons calculé le pourboire pour un repas et l'avons affiché à l'utilisateur :


| type      | name     | label                                 | calculation                  |
| --------- | -------- | ------------------------------------- | ---------------------------- |
| decimal   | amount   | What was the price of the meal?       |                              |
| calculate | tip      |                                       | ${amount} * 0.18             |
| note      | display  | 18% tip for your meal is: ${tip}      |                              |
| ========  | ======== | ===================================== | ============================ |
| survey    |          |                                       |                              |

Notez que le **${tip}** sur la dernière ligne sera remplacé par le montant réel du pourboire lorsque vous consulterez et remplirez le formulaire.

## Required (Requis)

Il est simple de marquer certaines questions comme requises dans votre formulaire.  Les marquer comme requises signifie que l'utilisateur ne sera pas en mesure de passer à la question suivante ou de soumettre le formulaire sans avoir entré une réponse à cette question.

Pour poser les questions requises, ajoutez une colonne **required** à votre feuille de calcul. Sous cette colonne, cochez les questions requises en écrivant **yes** comme dans l'exemple ci-dessous :

| type     | name     | label            | constraint | required   |
| -------- | -------- | ---------------- | ---------- | ---------- |
| integer  | age      | How old are you? | . <= 150   | yes        |
| ======== | ======== | ============     | ========== | ========== |
| survey   |          |                  |            |            |

### Required message

### Message requis

Si vous souhaitez personnaliser le message affiché aux utilisateurs lorsqu'ils laissent une question obligatoire vide, vous pouvez ajouter une colonne **required_message** à votre formulaire.
Voir l'exemple ci-dessous:

| type     | name           | label            | required   | required_message                |
| -------- | -------------- | ---------------- | ---------- | ------------------------------- |
| integer  | respondent_age | Respondent's age | yes        | Sorry, this answer is required. |
| ======== | ========       | ============     | ========== | =============================== |
| survey   |                |                  |            |                                 |

## Randomize Choices (Choix aléatoires)

Pour tout type de question qui affiche une **liste de choix**, l'ordre des choix affichés à l'utilisateur peut être aléatoire avec la colonne **parameters**. Voir ci-dessous :

| type                | parameters     | name  | label     |
| ------------------- | -------------- | ----- | --------- |
| select_one toppings | randomize=true | top   | Favorite? |
| ==================  | ============== | ===== | ========= |
| survey              |                |       |           |

Pour un tirage aléatoire reproductible, l'argument **seed** (graine) peut être fourni comme indiqué ci-dessous. Pour en savoir plus sur l'algorithme de tirage aléatoire utilisé, voir [ici](https://opendatakit.github.io/xforms-spec/#fn:randomize).

| type                | parameters                 | name | label       | calculation                    |
| ------------------- | -------------------------- | ---- | ----------- | ------------------------------ |
| calculate           |                            | sd   |             | once(decimal-date-time(now())) |
| select_one toppings | randomize=true, seed=${sd} | top  | Favorite?   |                                |
| ==================  | ========================== | ==== | =========== | ============================== |
| survey              |                            |      |             |                                |

Notez que `once()` est utilisé pour empêcher un nouveau tirage aléatoire quand, par exemple,  lun enregistrement brouillon est chargé pour être ré-édité.

## Regroupement de questions

Pour créer un groupe de questions essayez:

| type        | name       | label                                    |
| ----------- | ---------- | ---------------------------------------- |
| begin group | respondent | Respondent                               |
| text        | name       | Enter the respondent’s name              |
| text        | position   | Enter the respondent’s position within the school. |
| end group   |            |                                          |
| ========    | ========   | ============                             |
| survey      |            |                                          |

C'est un bon moyen de regrouper les questions relatives à l'exportation et à l'analyse des données. Notez que **end group** n'a pas besoin d'un nom ou d'une étiquette, car il est caché dans le formulaire.

### Groupes imbriqués dans des groupes

Des groupes de questions peuvent être imbriqués les uns dans les autres :

| type              | name                | label                                   |
| ----------------- | ------------------- | --------------------------------------- |
| begin group       | hospital            | Hospital                                |
| text              | name                | What is the name of this hospital?      |
| begin group       | hiv_medication      | HIV Medication                          |
| select_one yes_no | have_hiv_medication | Does this hospital have HIV medication? |
| end group         |                     |                                         |
| end group         |                     |                                         |
| ========          | ========            | ============                            |
| survey            |                     |                                         |

Vous devez toujours terminer en premier le groupe le plus récemment créé . Par exemple, le premier **end group** que vous voyez ferme le groupe *hiv_medication*, et le second **end group** ferme le groupe *hospital*. Lorsque vous travaillez avec des groupes et que vous continuez à recevoir des messages d'erreur lorsque vous essayez de télécharger votre formulaire, vérifiez par deux fois que pour chaque **begin group** vous avez bien un **end group**.

### Skipping (Sauter)

Une caractéristique intéressante de XLSForm est la possibilité de sauter un groupe de questions en combinant la fonction de groupe avec la syntaxe de **Relevant**. Si vous voulez sauter un groupe de questions d'un seul coup, placez l'attribut approprié au début d'un groupe comme suit :

| type              | name     | label                                    | relevant     |
| ----------------- | -------- | ---------------------------------------- | ------------ |
| integer           | age      | How old are you?                         |              |
| begin group       | child    | Child                                    | ${age} <= 5  |
| integer           | muac     | Record this child’s mid-upper arm circumference. |              |
| select_one yes_no | mrdt     | Is the child’s rapid diagnostic test positive? |              |
| end group         |          |                                          |              |
| ================= | ======== | ================================================= | ============ |
| survey            |          |                                          |              |

Dans cet exemple, les deux questions du groupe enfant (**muac** et **mrdt**) n'apparaîtront que si l'âge de l'enfant demandé dans la première question (**age**) est égal ou inférieur à cinq (``${age} <= 5``).

### Répétitions

Un utilisateur peut répéter un groupe de questions en utilisant la construction **begin repeat** et **end repeat** comme suit:

| type                   | name         | label               |
| ---------------------- | ------------ | ------------------- |
| begin repeat           | child_repeat |                     |
| text                   | name         | Child's name        |
| decimal                | birthweight  | Child's birthweight |
| select_one male_female | sex          | Child's sex         |
| end repeat             |              |                     |
| =================      | ========     | ==================  |
| survey                 |              |                     |

<p/>

| list name           | name        | label       |
| ------------------- | ----------- | ----------- |
| male_female         | male        | Male        |
| male_female         | female      | Female      |
| =================== | =========== | =========== |
| choices             |             |             |

Dans cet exemple, les champs **name** (nom), **birthweight** (poids de naissance) et **sex** sont regroupés dans un groupe de répétition, et l'utilisateur peut répéter ce groupe autant de fois qu'il le souhaite en sélectionnant l'option dans le formulaire pour commencer une autre répétition. 

La colonne **label** est facultative pour débuter la répétition avec **begin repeat**.  L'attribution d'une étiquette à un groupe de répétition ajoutera l'étiquette comme titre au bloc de questions répétitives dans le formulaire.

Le  XLSForm d'exemple intitulé  [Delivery Outcome](https://docs.google.com/spreadsheets/d/1_gCJml_FzJ4qiLU-yc67x1iu_GL-hfU3H8-HvINsIoE/edit?usp=sharing) illustre un autre exemple de groupe de répétition.

Au lieu de permettre un nombre infini de répétitions, l'utilisateur peut spécifier un nombre exact de répétitions en utilisant la colonne **repeat_count** :

| type                   | name         | label                | repeat_count |
| ---------------------- | ------------ | -------------------- | ------------ |
| begin repeat           | child_repeat |                      | 3            |
| text                   | name         | Child's name         |              |
| decimal                | birthweight  | Child's birthweight  |              |
| select_one male_female | sex          | Child's sex          |              |
| end repeat             |              |                      |              |
| =================      | ========     | ==================== | ============ |
| survey                 |              |                      |              |

<p/>

| list name           | name        | label       |
| ------------------- | ----------- | ----------- |
| male_female         | male        | Male        |
| male_female         | female      | Female      |
| =================== | =========== | =========== |
| choices             |             |             |

Dans l'exemple ci-dessus, le groupe de répétition est limité à **3** répétitions.  

Certaines plates-formes prennent également en charge le comptage dynamique des répétitions.  Dans l'exemple ci-dessous, le nombre que l'utilisateur saisit dans le champ **num_hhh_members** indique le nombre de fois que le groupe **hh_member** se répète : 

| type                   | name           | label                        | repeat_count      |
| ---------------------- | -------------- | ---------------------------- | ----------------- |
| integer                | num_hh_members | Number of household members? |                   |
| begin repeat           | hh_member      |                              | ${num_hh_members} |
| text                   | name           | Name                         |                   |
| integer                | age            | Age                          |                   |
| select_one male_female | gender         | Gender                       |                   |
| end repeat             |                |                              |                   |
| =================      | ========       | ====================         | ============      |
| survey                 |                |                              |                   |

<p/>

| list name           | name        | label       |
| ------------------- | ----------- | ----------- |
| male_female         | male        | Male        |
| male_female         | female      | Female      |
| =================== | =========== | =========== |
| choices             |             |             |

## Prise en charge multi-langues

Il est facile d'ajouter plusieurs langues à un formulaire. Il vous suffit de nommer votre **label::language1 (code)**, **label::language2 (code)**, etc. et vos formulaires seront disponibles en plusieurs langues. Voir l'exemple ci-dessous. Sélectionnez une langue de formulaire différente dans le menu déroulant de l'application de collecte de données (elle peut se trouver sous la touche **Menu**). Pour le formulaire ci-dessous, l'anglais et l'espagnol apparaîtront comme les options possibles.

| type              | name        | label::English (en) | label::Español (es)   | constraint |
| ----------------- | ----------- | ------------------- | --------------------- | ---------- |
| integer           | age         | How old are you?    | ¿Cuántos años tienes? | . <= 150   |
| ================= | =========== | ============        | ============          | ========== |
| survey            |             |                     |                       |            |

La langue du formulaire et la langue de l'interface utilisateur peuvent être déterminées séparément par l'application et peuvent ne pas correspondre. Pour faciliter l'appariement des deux (à l'avenir), il est recommandé, quoique facultatif, d'ajouter un code de langue à 2 caractères après le nom de la langue. Les codes de langue officiels à 2 caractères, appelés _subtags_, sont publiés[ici] (http://www.iana.org/assignments/language-subtag-registry/language-subtag-registry) (recherchez la page avec Ctrl-F ou Cmd-F).

**Note**

Vous pouvez également ajouter une colonne de langue différente pour les astuces et les fichiers multimédia ; il vous suffit d'utiliser à nouveau la construction ``::language``. Voir le  [XLSForm standard document](https://docs.google.com/spreadsheet/ccc?key=0AjZ4hMHTat-YdFZSY3BOZWtGeTdxWGQ1clZoVEZkamc&usp=sharing) pour voir exactement quels types de titres de colonne peuvent accepter une modification de langue.

## Media

Vous pouvez inclure dans votre formulaire des questions qui affichent des images ou qui lisent des fichiers vidéo ou audio. Si vous utilisez le client mobile ODK pour soumettre un formulaire, vous devez placer les fichiers médias que vous voulez inclure dans le dossier **/odk/forms/nomduformulaire-media** sur votre téléphone, puis indiquer le nom exact du fichier dans la colonne **media** de votre formulaire. Voir ci-dessous un exemple de la façon de procéder.

| type              | name          | label         | media::image | media::video |
| ----------------- | ------------- | ------------- | ------------ | ------------ |
| note              | media_example | Media example | example.jpg  | example.mp4  |
| ================= | ===========   | ============  | ============ | ==========   |
| survey            |               |               |              |              |

Consultez le[Birds XLSForm] (https://docs.google.com/spreadsheets/d/1Rxft3H3xl3M9bLFGR2XhXzt1ucyFmd0qFmOQ6FaqJw4/edit?usp=sharing) qui illustre l'utilisation des fichiers médias. Vous pouvez également cliquer sur le lien pour voir le[Formulaire Web pour les oiseaux] (https://enketo.ona.io/x/#Ynv3).

## Préchargement des données CSV

Le préchargement des données se fait lorsque l'on veut référencer des données préexistantes dans un formulaire d'enquête. Vous pouvez référencer des données dans votre formulaire d'enquête (l'enquête que vous êtes en train de rédiger), à partir de données préexistantes dans un formulaire d'enquête spécifique ou de toute autre source.  Par exemple, si vous disposez de données préexistantes provenant d'une enquête sur les ménages et que vous souhaitez collecter des données de suivi sur les occupants du ménage. Vous pouvez faire référence aux données de l'enquête auprès des ménages dans votre formulaire d'enquête.
Pour référencer des données préexistantes dans un formulaire d'enquête :

* Téléchargez un ou plusieurs fichiers.csv en tant que fichiers de support lorsque vous téléchargez votre définition de formulaire (de la même manière que vous téléchargez des fichiers de support média comme expliqué dans la section [Media](#media)). la première ligne de chaque fichier.csv doit être un en-tête qui comprend un court :
  * noms uniques pour chaque colonne
  * les lignes suivantes doivent contenir les données elles-mêmes

Chaque fichier csv doit contenir au moins une colonne qui peut être utilisée d'identifiant unique. Ces colonnes seront utilisées, au moment de l'enquête, pour rechercher la ligne de donnée qui sera utilisée dans l'enquête. Pour les colonnes qui contiennent les identifiants uniques, ajoutez **_key** à la fin du nom de la colonne dans la première ligne. Toutes les colonnes dont le nom se termine par **_key** seront indexées pour une consultation plus rapide sur vos appareils contenant les formulaires d'enquête. Voir ci-dessous un exemple des colonnes d'un fichier.csv :

| name_key | name   |
| -------- | ------ |
| mango    | Mango  |
| orange   | Orange |

## Pre-charger des données CSV

Le préchargement des données se fait lorsque l'on veut référencer des données préexistantes dans un formulaire d'enquête. Vous pouvez référencer des données dans votre formulaire d'enquête (l'enquête que vous êtes en train de rédiger), à partir de données préexistantes dans un formulaire d'enquête spécifique ou de toute autre source. Par exemple, si vous disposez de données préexistantes provenant d'une enquête sur les ménages et que vous souhaitez collecter des données de suivi sur les occupants du ménage. Vous pouvez faire référence aux données de l'enquête auprès des ménages dans votre formulaire d'enquête. Pour référencer des données préexistantes dans un formulaire d'enquête :

* Téléchargez un ou plusieurs fichiers .csv en tant que fichiers de support lorsque vous téléchargez votre formulaire de "définition" (de la même manière que vous téléchargez des fichiers de support média comme expliqué dans la section[Media](#media)). la première ligne de chaque fichier .csv doit être une en-tête qui comprend :
  * noms uniques et courts pour chaque colonne
  * les lignes suivantes qui doivent contenir les données elles-mêmes

Chaque fichier csv doit contenir au moins une colonne d'identifiant qui peut être utilisée pour identifier chaque ligne de manière unique. Ces colonnes seront utilisées, au moment de l'enquête, pour rechercher quelle ligne de données doit être incluse dans le formulaire d'enquête. Pour les colonnes d'identifiant ajoutez **_key** à la fin du nom de la colonne. Toutes les colonnes dont le nom se termine par **_key** seront indexées pour une consultation plus rapide sur vos appareils de sondage. Voir ci-dessous un exemple des colonnes d'un fichier .csv :

| name_key | name   |
| -------- | ------ |
| mango    | Mango  |
| orange   | Orange |

### Comment extraire des données de CSV

Vous pouvez extraire des données d'un fichier .csv en incluant un ou plusieurs fichiers.csv dans votre formulaire pendant la durée du sondage. 
Pour chaque champ de données que vous voulez inclure dans votre enquête :

 * Ajoutez un champ **de calcul** à votre enquête. 
 * Donner un ** nom** a ce champ
 * Puis dans sa colonne **calculation**, appelez la fonction **pulldata()**, en indiquant quel champ extraire de quelle ligne de quel fichier.csv.

Voir ci-dessous pour un exemple :

| type      | name   | label   | calculation               |
| --------- | ---------- | ----- | ------------------------- |
| calculate | fruit  | | pulldata('fruits', 'name', 'name_key', 'mango')|
| note      | note_fruit | The fruit ${fruit} pulled from csv. |      |
| ========= | ========== | ================== | ======== |
| survey    |            |           |                              |

Une fois que vous avez chargé des données.csv dans un champ d'enquête à l'aide de la fonction **pulldata()**, vous pouvez faire référence à ce champ par la suite, comme vous le feriez avec tout autre champ rempli par l'utilisateur, dans des conditions, contraintes (**relevant**) ou étiquettes (**label**). 

Cliquez sur le lien pour voir un exemple d'un exemple de[préchargement d'un exemple de formulaire ](https://docs.google.com/spreadsheets/d/1evieF8RW8CMlhbhksgfikXAYvK6uXh3DS5c50ejTSEw/edit?usp=sharing) et le fichier.csv utilisé avec le formulaire [ici](https://docs.google.com/spreadsheets/d/1gprb7ocTYlT_seOBFY5CuoxyodcXwWOuVxmp38OX1dE/edit?usp=sharing)

**Remarques importantes sur l'utilisation des données préchargées**

 * Compressez un gros fichier.csv dans une archive **.zip** avant de le télécharger.
 * Enregistrer le fichier.csv au format **UTF-8** si les données préchargées contiennent des polices non anglaises ou des caractères spéciaux, cela permet à votre appareil Android de rendre le texte correctement. 
 * Les champs de données extraits d'un fichier.csv sont considérés comme des chaînes de texte, donc utilisez les fonctions **int()** ou **number()** pour convertir un champ préchargé en format numérique.
 * Si le fichier.csv contient des données sensibles que vous ne souhaitez peut-être pas télécharger sur le serveur, téléchargez un fichier.csv vierge dans le cadre de votre formulaire, puis remplacez-le par le vrai fichier.csv en copiant manuellement le fichier sur chacun de vos appareils.


## Sélection dynamique à partir de données préchargées
Une fois que votre formulaire a un ou plusieurs fichiers .csv préchargés, vous pouvez extraire dynamiquement les listes de choix des champs **select_one** et **select_multiple** de ces fichiers .csv.  Les champs à choix multiples avec des listes de choix dynamiques suivent la même syntaxe générale que les champs à choix multiples réguliers, statiques **select_one** et **select_multiple** comme décrit précédemment dans la section[Questions à choix multiples](#multiple-choice).

Il faut:

Une fois que votre formulaire a un ou plusieurs fichiers .csv préchargés, vous pouvez extraire dynamiquement les listes de choix des champs **select_one** et **select_multiple** de ces fichiers .csv.  Les champs à choix multiples avec des listes de choix dynamiques suivent la même syntaxe générale que les champs à choix multiples réguliers, statiques **select_one** et **select_multiple** comme décrit précédemment dans la section[Questions à choix multiples](#multiple-choice).

Ce qui suit devrait être fait :

* spécifier **select_one listname** ou **select_multiple listname** dans la colonne type (où **listname** est le nom de votre liste de choix)
* Spécifiez tous les styles  **d'apparence spéciale** dans une colonne **appearance**.
* inclure une ou plusieurs lignes pour votre nom de liste sur la feuille de calcul des choix. 

Vous trouverez ci-dessous un exemple de la feuille  **survey** :

| type              | name   | label          | appearance       |
| ----------------- | ------ | -------------- | ---------------- |
| select_one fruits | fruits | Select a fruit | search('fruits') |
| ================= | ====== | ============== | ================ |
| survey            |        |                |                  |

Il y a trois différences quand la liste de choix doit être extraite d'un de vos fichiers .csv préchargés :

* Dans la colonne **appearance **:
 * Inclure une expression **search()** qui spécifie les lignes .csv à inclure dans la liste de choix.
 * Si le champ doit utiliser un style d'apparence autre que par défaut. Le style d'apparence spécial va d'abord dans la colonne, suivi d'un **espace**, puis de l'expression **search()**. [**quick search()**]
* Sur la feuille de travail **choices** :
 * une ligne devrait indiquer quelles colonnes .csv utiliser pour l'étiquette et la valeur sélectionnée. Comme suit :
   * colonne **list_name**  : spécifiez le nom de votre liste de choix comme vous le feriez normalement.

   * colonne **name** : inclut le nom de la colonne .csv à utiliser pour identifier de façon unique les choix sélectionnés.

    * colonne **label** : inclut le nom de la colonne .csv à utiliser pour étiqueter les choix.



**Note** :

Si vous souhaitez inclure plusieurs colonnes dans les étiquettes, incluez une liste de toutes les colonnes à inclure séparées par des virgules. La colonne nom sera remplie dynamiquement en fonction du nom de la colonne que vous avez mis, et la colonne **label** sera remplie dynamiquement en fonction du ou des noms de colonne que vousavez mis.

Dans la ligne de la feuille *choices*, vous pouvez également inclure un nom de colonne.csv dans le champ *image*. Si vous le faites, le nom de fichier image à utiliser sera extrait de la colonne .csv spécifiée. 

**Note** :

Si vous vous référez aux fichiers image de cette façon, vous devez toujours télécharger ces fichiers image en tant que pièces jointes au fichier multimédia lorsque vous téléchargez votre formulaire sur le serveur.

Voir ci-dessous un exemple de feuille *choices* :

| list name         | name     | label          |
| ----------------- | -------- | -------------- |
| fruits            | name_key | name           |
| ================= | ======   | ============== |
| choices           |          |                |

Cliquez sur le lien pour voir un exemple de[formulaire de "recherche et de sélection" (*search-and-select*)] (https://docs.google.com/spreadsheets/d/1Y0vW0cjl1nbkZczXRmcTC71Pso8dRbouPSYWGBdvBWU/edit?usp=sharing) et le fichier .csv utilisé avec le formulaire [ici] (https://docs.google.com/spreadsheets/d/1gprb7ocTYlT_seOBFY5CuoxyodcXwWOuVxmp38OX1dE/edit?usp=sharing).

Il y a une série d'options pour indiquer quelles lignes .csv inclure dans la liste de choix en utilisant l'expression **search()**, voir [ici](http://opendatakit.org/help/form-design/data-preloading/) pour des informations supplémentaires sur ces expressions **search()**. 

## La sélection en cascade

Beaucoup de formulaires commencent par demander l'emplacement, chaque sélection d'emplacement définissant une localisation ultérieure (p. ex., pays >> région>> département>>ville).  Au lieu d'ajouter un champ **select_one** pour chaque option de localisation, vous pouvez utiliser la sélection en cascade. Pour utiliser les sélections en cascade, vous devrez créer une colonne **choice_filter** dans votre feuille de calcul et ajouter les colonnes d'attributs d'emplacement dans la feuille *choices*. Consultez un exemple de XLSForm [ici](https://docs.google.com/spreadsheet/ccc?key=0AjZ4hMHTat-YdFVpOWVBVWREUGdNVWZKbUl2akhfWkE&usp=sharing)

## Sélections externes

Si un formulaire a un grand nombre de choix (par exemple, des centaines ou des milliers), ce formulaire peut ralentir le chargement et la navigation dans des clients comme ODK Collect. La meilleure solution à ce problème est d'utiliser des sélections externes.

L'activation de sélections externes est très simple. 
 - Au lieu d'utiliser une invite de type  **select_one**, utilisez **select_one_external**.
 - Au lieu de la feuille **choices**, placez les choix externes dans la feuille **external_choices**.

Voir le formulaire[select_one_external](https://docs.google.com/spreadsheets/d/12qZL34kuHSZGWDv0BBJ1qf7dSmml-d2VnMWH0Vtg-O4/edit?usp=sharing) pour un exemple qui utilise des choix normaux et des choix externes.

Lorsqu'un XLSForm avec des choix externes est converti en XForm, deux fichiers seront produits, le **XForm** (par exemple, form-filename.xml) avec tous les choix normaux, et un **itemsets.csv** avec les choix externes. 

Le fichier **itemsets.csv** peut être téléchargé vers n'importe quel serveur compatible ODK (ODK Aggregate par ex.) sous forme de fichier multimédia. Il sera téléchargé dans n'importe quel fichier compatible ODK (par exemple, ODK Collect) comme n'importe quel autre fichier média et enregistré dans le dossier media [form-filename]. Les clients comme ODK Collect chargent les fichiers multimédias à partir de la carte SD et votre formulaire avec un grand nombre de choix se chargera maintenant très rapidement.

## Par défaut

L'ajout d'un champ **défault** permet de pré-remplir une question avec une réponse lorsque l'utilisateur voit la question pour la première fois.  Cela peut aider à gagner du temps si la réponse est une réponse qui est couramment choisie ou elle peut servir à montrer à l'utilisateur quel type de réponse est attendu.  Voir les deux exemples ci-dessous.

| type              | name        | label          | default          |
| ----------------- | ----------- | -------------- | ---------------- |
| today             | today       |                |                  |
| date              | survey_date | Survey date?   | 2010-06-15       |
| ================= | ======      | ============== | ================ |
| survey            |             |                |                  |

Dans l'exemple suivant, le poids est automatiquement réglé à 51,3 kg.  Vous pouvez simplement modifier la réponse en tapant sur le champ de réponse et en entrant une autre réponse.

| type              | name   | label                         | default          |
| ----------------- | ------ | ----------------------------- | ---------------- |
| decimal           | weight | Respondent's weight? (in kgs) | 51.3             |
| ================= | ====== | ==============                | ================ |
| survey            |        |                               |                  |

## Lecture seule

L'ajout d'un champ **read_only** (lecture seule) signifie qu'une question ne peut pas être modifiée. Les champs en lecture seule peuvent être combinés avec les champs par défaut pour fournir des informations à un utilisateur. 

| type      | name   | label              | read_only        | default |
| --------- | ------ | ------------------ | ---------------- | ------- |
| integer   | num    | Please patient is: | yes              | 5       |
| ========= | ====== | ==============     | ================ |         |
| survey    |        |                    |                  |         |

## Apparence

La colonne **appearance** vous permet de modifier l'apparence des questions dans votre formulaire. Le tableau suivant répertorie les attributs d'apparence possibles et la façon dont la question apparaît dans le formulaire.


| Appearance attribute | Question type               | Description                                                  |
| -------------------- | --------------------------- | ------------------------------------------------------------ |
| multiline            | text                        | Idéal pour une utilisation avec un si utilisé clients Web, rend la boîte de texte multiligne |
| minimal              | select_one, select_multiple | Les choix de réponses apparaissent dans un menu déroulant.   |
| quick                | select_one                  | Pertinent pour les clients mobiles uniquement, cet attribut fait automatiquement passer le formulaire à la question suivante après la sélection d'une réponse. |
| no-calendar          | date                        | Pour les appareils mobiles uniquement, utilisé pour supprimer le calendrier. |
| month-year           | date                        | SSélectionnez un mois et une année seulement pour la date.   |
| year                 | date                        | Sélectionnez seulement une année pour la date.               |
| horizontal-compact   | select_one, select_multiple | Pour les clients Web uniquement, cette option affiche les choix de réponse horizontalement. |
| horizontal           | select_one, select_multiple | Pour les clients Web uniquement, cette option affiche les choix de réponse horizontalement, mais en colonnes. |
| likert               | select_one                  | Mieux si utilisé avec des clients Web, fait apparaître les choix de réponse comme une échelle de Likert. |
| compact              | select_one, select_multiple | Affiche les choix de réponses côte à côte avec un espacement minimal et sans cases à cocher ou boutons radio. Particulièrement utile pour le choix des images. |
| quickcompact         | select_one                  | Comme le précédent mais passe automatiquement à la question suivante (appareils mobiles uniquement). |
| field-list           | groups                      | Un groupe entier de questions apparaît sur un seul écran (appareils mobiles uniquement). |
| label                | select_one, select_multiple | Affiche les étiquettes de choix de réponse (et non les entrées). |
| list-nolabel         | select_one, select_multiple | Utilisé avec l'attribut **label** ci-dessus, affiche les entrées de réponse sans les étiquettes (assurez-vous de placer les champs **label** et **list-nolabel** dans un groupe avec l'attribut **field-list** si vous utilisez un appareil mobile). |
| table-list           | groups                      | Pour obtenir plus facilement la même apparence que ci-dessus, appliquez cet attribut à l'ensemble du groupe de questions (peut ralentir un peu le formulaire). |
| signature            | image                       | Permet de tracer votre signature dans votre formulaire (clients mobiles uniquement). |
| draw                 | image                       | Permet de dessiner avec vos doigts sur l'écran de l'appareil mobile. |

Un formulaire XLSForm avec tous les attributs d'apparence de ce tableau est disponible [ici](https://docs.google.com/spreadsheets/d/159tf1wNeKGRccgizZBlU3arrOM--OpxWo26UvZcDEMU/edit?usp=sharing).

## paramètres des feuille de travail

La feuille de travail **settings** est facultative, mais elle vous permet de personnaliser davantage votre formulaire, notamment en cryptant vos enregistrements ou en définissant un thème de style général pour votre formulaire.

Vous trouverez ci-dessous un exemple de feuille de travail **settings** :

| form_title | form_id | public_key     | submission_url                                       | default_language  | version      |
| ---------- | ------- | -------------- | ---------------------------------------------------- | ----------------- | ------------ |
| Example    | ex_id   | IIBIjANBg...   | https://example.com/submission | English           | 2017021501   |
| ========   | ======  | ============== | ================                                     | ================= | ============ |
| settings   |         |                |                                                      |                   |              |

Les en-têtes de colonne de cet exemple de feuille de travail **settings** font ce qui suit :

* **form_title** : Le titre du formulaire qui est montré aux utilisateurs. Le titre du formulaire est extrait de **form_id** si **form_title** est vide ou manquant.
* **form_id** : Le nom utilisé pour identifier de façon unique le formulaire sur le serveur. L'identifiant du formulaire est extrait du nom de fichier XLS si **form_id** est vide ou manquant.
* **clé_publique** : Pour les formulaires cryptés, c'est ici que la clé publique est copiée et collée.
* **submission_url** : Cette url peut être utilisée pour remplacer le serveur par défaut sur lequel les enregistrements finalisés sont envoyés.
* **langue_par_défaut** : Dans les formulaires localisés, cette option définit la langue qui doit être utilisée par défaut.
***version** : Chaîne d'un maximum de 10 chiffres qui décrit cette révision. Les définitions de formulaire révisées doivent avoir des versions numériques plus grandes que les précédentes. Une convention courante consiste à utiliser des chaînes de caractères de la forme " yyyyyymmddrr ". Par exemple, 201702150101 est la 1ère révision du 15 février 2017.

### Formulaires cryptés

Les formulaires cryptés fournissent un mécanisme permettant de garder les dossiers _finalisés_ privés en permanence. Ceci inclut le temps _après qu'un enregistrement soit marqué comme finalisé_ qu'il soit stocké sur l'appareil et le serveur ainsi que le temps de transport, même lorsque http est utilisé pour communiquer. Les enregistrements cryptés, y compris les fichiers téléchargés, tels que les photos, sont totalement inaccessibles à toute personne ne possédant pas la clé privée.

Pour crypter les formulaires XLS, ajoutez la colonne **public_key** à la feuille de calcul **settings** et collez la clé RSA publique codée en base64 comme valeur.


| form_id | public_key     |
| ------- | -------------- |
| mysurvey   | IIBIjANBgklawWEserewrwesgdreewrwe32serfserfewrwerewtwer23sgfrqjwerk3423432...   |
| ======  | ============== |
| settings   |         |

Pour plus d'informations sur les formulaires cryptés et la manière de générer les clés RSA, consultez la [documentation ODK](https://docs.opendatakit.org/encrypted-forms/) et [cet exemple de formulaire](https://docs.google.com/spreadsheets/d/1O2VW5dNxXeyr-V_GB3spS6QPX4rtqtt7ijqP_uZLU3I/edit?usp=sharing).

Regardez cet[exemple XLSForm](https://docs.google.com/a/ona.io/spreadsheets/d/1Bapi05GYITV6D0THvs9RaOSEjqb1-FWIhfjSTXtvl8I/edit#gid=1932003628) qui calcule le nom de l'instance avec le nom et le prénom de l'utilisateur couplé avec le uuid de soumission du formulaire.

### Formulaires Web  multi-pages 

Les formulaires Web peuvent être divisés en plusieurs pages en utilisant le thème de style **pages**.

Un exemple de formulaire divisé en plusieurs pages peut être vu  ici [Widgets on pages] (https://enketo.ona.io/x/#YjeC).

Dans l'onglet **réglages**, créez une colonne appelée **style** et réglez-la sur **pages**, comme suit :

| form_title    | form_id    | style  |
| ------------- | ---------- | ------ |
| example title | example_id | pages  |
| =========     | ========   | ====== |
| settings      |            |        |

Dans votre onglet **survey**, regroupez les questions que vous souhaitez voir apparaître sur chaque page, puis définissez l'apparence du groupe sur **field-list**. Voir l'exemple ci-dessous:


| type        | name     | label                | appearance |
| ----------- | -------- | -------------------- | ---------- |
| type        | name     | label                | appearance |
| begin group | group1   |                      | field-list |
| text        | name     | Respondent's name    |            |
| integer     | age      | Respondent's age     |            |
| string      | address  | Respondent's address |            |
| end group   |          |                      |            |
| =========   | ======== | ======               | ====       |
| survey      |          |                      |            |

Voir ce [billet de blog](http://blog.enketo.org/pages/) pour plus d'informations sur la création de formulaires Web multi-pages.  Le code source de XLSForm est [ici](https://docs.google.com/spreadsheets/d/1yZqG2Xt0I4duVxPqx-Sny0t86OiKtjHuBKXTRzCht6E/edit?usp=sharing.).

### Formulaires avec le thème en "grille"

Le style **theme-grid** permet à votre formulaire d'imiter l'aspect des enquêtes papier traditionnelles en compactant plusieurs questions sur une seule rangée. Ce style s'utilise de préférence avec de plus grands écrans (p. ex., ordinateurs ou tablettes).  Il fait aussi une belle impression !

Veuillez cliquer sur le lien pour voir un exemple de[Formulaire Web du thème de la grille] (https://enketo.ona.io/x/#Yn4R).

Pour créer un formulaire Grille, dans l'onglet **settings**, sous la colonne **style**, écrivez **theme-grid**, comme suit :

| form_title    | form_id    | style      |
| ------------- | ---------- | ---------- |
| example title | example_id | theme-grid |
| =========     | ========   | ======     |
| settings      |            |            |

Dans votre onglet **survey**, regroupez les questions que vous souhaitez voir apparaître dans chaque section, puis définissez l'apparence de chaque champ en fonction de la largeur souhaitée (la largeur par défaut est 4). Voir l'exemple ci-dessous:


| type        | name     | label                | appearance |
| ----------- | -------- | -------------------- | ---------- |
| begin group | group1   |                      |            |
| text        | name     | Respondent's name    | w3         |
| integer     | age      | Respondent's age     | w1         |
| string      | address  | Respondent's address | w4         |
| end group   |          |                      |            |
| =========   | ======== | ======               | ====       |
| survey      |          |                      |            |

Voir ce [billet de blog](http://blog.enketo.org/gorgeous-grid/) pour plus d'informations sur la création de formulaires de grille. L'exemple du thème de grille XLSForm est [ici] (https://docs.google.com/spreadsheets/d/1Z4gHZQTr5FibRK-Aj198WlNdMZghEBZlyWhmPZXjzJQ/edit?usp=sharing). 

## Invites de style

La prise en charge du **Markdown** dans XLSForm permet de mettre davantage en évidence des mots avec des caractères gras et italiques, des en-têtes de tailles différentes, des polices et des couleurs diverses, ainsi que des liens Web cliquables dans ODK Collect 1.4.9 et Enketo.

* * *accentuer* les mots en les enveloppant à l'intérieur de `_` ou `**`.
* Mettre fortement l'accent sur les mots en les enveloppant à l'intérieur de `__` ou `****`.
* ajouter un lien en utilisant `[nom du lien](url)` `.
* ajouter des en-têtes de différentes tailles en faisant précéder # (<big>plus grand</big>) à ######## (<smallest>plus petit</small>) au texte d'en-tête
* Ajouter au texte de la couleur ou changer de police avec des balises (par ex., `<span style="color:#f58a1f">`<span style="color:#f58a1f">orange</span>`</span>`, `<span style="color:red; font-family:cursive">`<span style="color:red; font-family:cursive;">red and cursive</span>`</span>`)
* ajouter un saut de ligne là où vous le souhaitez avec Ctrl-Enter ou Ctrl-Alt-Enter (peut être une combinaison de touches différente pour certains tableurs)
* Ajoutez vos émojis préférés 😍📋😍 !
* utiliser les exposants avec la balise `<sup>` (par exemple `100 m<sup>2</sup>` se transforme en 100 m<sup>2</sup>)
* utiliser les indices avec la balise `<sub>` (par exemple `H<sub>2</sub>O` se transforme en H<sub>2</sub>O)
* utiliser le caractère `\\` avant `#`, `*`, `_`, `_` et ``\` pour éviter que des effets de style spéciaux Markdown ne soient déclenchés par ces caractères.

## Utilisation avancée et extensibilité

Il est possible d'utiliser XLSForm pour créer des XForms avec des fonctionnalités personnalisées ou expérimentales. C'est idéal pour les applications personnalisées avec une fonctionnalité spécifique qui n'est pas adaptée à l'ensemble de la communauté. 

La feuille **survey** prend en charge 3 préfixes de colonnes (**instance::**, **bind::**, **body::**, **body::**) qui ajoutent des attributs à la sortie XForm, soit dans _primary instance_, _bind_ ou _form control_. Pour en savoir plus sur les XForms, consultez [ODK XForms Specification] (https://opendatakit.github.io/xforms-spec/). L'exemple ci-dessous ajoute un attribut "hxl" personnalisé au noeud d'instance primaire d'une question:

| type      | name       | label                    | instance::hxl |
| --------- | ---------- | ------------------------ | ------------- |
| integer   | population | How many people present? | #population   |
| ========= | ========   | ======================== | ============= |
| survey    |            |                          |               |

La feuille **settings** prend en charge la possibilité de définir des *namespace* additionels (il faut alors les séparer par plusieurs espaces) et des préfixes de *namespace* en utilisant la colonne **namespaces**. Vous pourrez alors utiliser ces *namespace* dans la feuille de sondage, pour par ex. définir correctement un attribut personnalisé avec [le noms de votre organisation] (https://github.com/opendatakit/xforms-spec#specification-changes). Voir l'exemple ci-dessous qui ajoute 2 espaces de noms supplémentaires et les utilisent pour ajouter des attributs personnalisés :

| title     | namespaces                                                   |
| --------- | ------------------------------------------------------------ |
| My Form   | esri="http://esri.com/xforms" enk="http://enketo.org/xforms  |
| ========= | ============================================================ |
| settings  |                                                              |

| type      | name         | label                    | bind::esri:fieldLength | bind::enk:for |
| --------- | ------------ | ------------------------ | ---------------------- | ------------- |
| text      | desc         | Describe                 | 50                     |               |
| text      | desc_comment | Comments                 |                        | ${a}          |
| ========= | ============ | ======================== | ====================== | ============= |
| survey    |              |                          |                        |               |

## Outils qui supportent XLSForms
* [Ona](http://ona.io)
* [Enketo](http://enketo.org)
* [Open Data Kit (ODK)](http://opendatakit.org)
* [Kobo ToolBox](http://kobotoolbox.org)
* [CommCare](http://commcarehq.org)
* [SurveyCTO](http://www.surveycto.com/)
* [DataWinners](https://www.datawinners.com/en/home/)
* [Secure Data Kit (SDK)](http://www.securedatakit.com)
* [Tattara](http://tattara.com/)

## Plus de ressources

Le  [XLSform standard document](https://docs.google.com/spreadsheet/ccc?key=0AjZ4hMHTat-YdFZSY3BOZWtGeTdxWGQ1clZoVEZkamc&usp=sharing)  peut vous guider à travers les types de saisies spécifiques, les en-têtes de colonne, etc. les syntaxes appropriées dans XLSForms. Si vous voulez creuser plus profondément pour comprendre les XForms et aller au-delà de XLSForms, voici quelques ressources pour les comprendre :

* Form guidelines: [http://code.google.com/p/opendatakit/wiki/XFormDesignGuidelines](http://code.google.com/p/opendatakit/wiki/XFormDesignGuidelines)
* Form design tutorial: [https://bitbucket.org/javarosa/javarosa/wiki/buildxforms](https://bitbucket.org/javarosa/javarosa/wiki/buildxforms)
* Sample forms: [http://code.google.com/p/opendatakit/source/browse/?repo=forms](http://code.google.com/p/opendatakit/source/browse/?repo=forms)
* [Formhub University](http://formhub.org/formhub_u)
* [Ona Form Gallery](http://ona.io)
* XForms as supported by JavaRosa: [https://bitbucket.org/javarosa/javarosa/wiki/xform-jr-compat](https://bitbucket.org/javarosa/javarosa/wiki/xform-jr-compat)

## A propos de ce site

XLSForm.org est un projet soutenu par la communauté visant à créer un point de référence commun pour la norme XLSForm.

Si vous souhaitez contribuer à cette documentation ou l'améliorer, veuillez consulter notre[projet GitHub repo] (https://github.com/XLSForm/xlsform.github.io).

## Histoire

Le XLSForm a été développé à l'origine par Andrew Marder et Alex Dorey du[Sustainable Engineering Lab at Columbia University] (http://sel.columbia.edu).  Au fur et à mesure que XLSForms a été adopté par la communauté ODK, SEL a travaillé avec l'équipe ODK de l'Université de Washington pour développer la spécification actuelle.  [PyXForm](https://github.com/XLSForm/pyxform), la bibliothèque utilisée pour convertir XLSForms en XForms, est un projet open source supporté par les membres d'ODK, SEL, Ona, SurveyCTO, et Kobo.

[retour à la page d'Accueil](README.md)





